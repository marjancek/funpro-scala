package funsets

object Main extends App {
  import FunSets._
  println(contains(singletonSet(1), 3))
  val s1 = singletonSet(1)
  val s2 = singletonSet(3)
  printSet(union(s1,s2))

  val a = union(singletonSet(1),union(singletonSet(3), union(singletonSet(4), union(singletonSet(5), union(singletonSet(7),singletonSet(1000))))))
  val b = union(union(singletonSet(1), singletonSet(2)), union(singletonSet(3), singletonSet(4)))
  val c = union(singletonSet(-1000), singletonSet(0))


  printSet(a)
  printSet(b)

  println("union")
  printSet(union(a,b))
  println("diff")
  printSet(diff(a,b))
  println("intersect")
  printSet(intersect(a,b))

  println("-------")
  printSet(b)
  printSet(c)
  println("diff")
  printSet(diff(b,c))


  println("filter odds")
  printSet( filter(a, (x:Int)=> (x%2>0)) )

  println( "All positive: " + forall(a, x => x > 0))
  println( "All odd: " + forall(a, (x:Int)=> (x%2 > 0) ))

  println( "has a negative: " + exists(a, x => x < 0))
  println( "has an odd: " + exists(a, (x:Int)=> (x%2>0) ))

  println("Set squared")
  printSet(b)
  printSet(map(a, x => x*x ))
}
