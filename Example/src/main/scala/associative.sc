val romanNUmerals = Map("I"-> 1, "V"-> 5, "X"->10)
val capitalOfCountries = Map("Slovenia"->"Ljubljana", "Croatia"->"Zagreb", "Italy"->"Rome")

//capitalOfCountries("Argentina")

capitalOfCountries get "Argentina"
capitalOfCountries get "Slovenia"



def showCapital(country:String) : String = capitalOfCountries.get(country) match {
  case Some(capital) => capital
  case None => "I don't know"
}

showCapital("Argentina")
showCapital("Slovenia")

val fruit = List("Apple","Cherry", "Banana", "Apricot")

fruit sortWith (_.length<_.length)

fruit.sorted

fruit groupBy(_.head)
fruit groupBy(_.length)

capitalOfCountries.getOrElse("Argentina", "who knows")
capitalOfCountries.getOrElse("Slovenia", "who knows")