// Peano numbers

abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def + (that:Nat): Nat
  def - (that:Nat): Nat
  def toInt: Int
}

class Zero extends Nat  {
  def isZero: Boolean = true
  def predecessor: Nat = throw new NoSuchElementException
  def + (that:Nat): Nat = that
  def - (that:Nat): Nat = {
    if( that.isZero ) this
    else throw new NoSuchElementException
  }
  def toInt: Int = 0
}

class Succ(n:Nat) extends Nat {
  def isZero: Boolean = false

  def predecessor: Nat = n

  def + (that:Nat): Nat = {
    if(that.isZero) this
    else this.successor + that.predecessor // new Succ(n+that)
  }

  def - (that:Nat): Nat = {
    if(that.isZero) this
    else this.n - that.predecessor // new Succ(n+that)
  }

  def toInt: Int = predecessor.toInt + 1
}


object naturals {
  def main(args: Array[String]) = {
    val z = new Zero
    val x = new Succ(z)

    val y = x.successor.successor.successor.successor.successor.successor
    println("Testing")
    println(y.toInt)
  }
}

//naturals.main( new Array[String](0) )  // run main without parameters
val z = new Zero
val z2 = new Zero

val x = new Succ(z)
val y = x.successor.successor.successor.successor.successor.successor

val w = z2.successor.successor.successor.successor

println("Testing")
println(y.toInt)
println(w.toInt)
println(x.toInt)

(z2+x+w+y+z+x).toInt