val v = Vector(1,2,3,4)
val w = Vector(3,1,2,4)

1 +: v

val r: Range = 1 until 5
val s: Range = 1 to 5
val t = 1 to 3 by 3
val z = 6 to 0 by -2

z.tail


def scalarProduct(xs:Vector[Int], ys:Vector[Int]): Int =
  (xs zip ys).map(xy=>xy._1 * xy._2).sum

(v zip w) map(p=>p._1+p._2)

scalarProduct(v, w)

def isPrime (n:Int): Boolean =
  (2 until n).forall( x => n % x > 0)

isPrime(2)
isPrime(3)
isPrime(4)
isPrime(5)
isPrime(6)
isPrime(7)
isPrime(8)

def primeSumTo(n:Int) = ((1 until n) flatMap (i => (1 until i) map (j => (i,j) )))   filter (p => isPrime(p._1 + p._2))
//(((1 until n) map (i => (1 until i) map (j => (i,j) ))) flatten ) filter (p => isPrime(p._1 + p._2))

primeSumTo(10)

case class Person(name:String, age:Int)

val persons = List(Person("A", 20), Person("B", 30), Person("C", 40))

for (p<-persons if p.age>20) yield p.name

persons filter (p => p.age > 20) map (p=>p.name)

def primeSumTo2(n:Int) =
for {
  i<- 1 until n
  j<- 1 until i
  if isPrime(i+j)
} yield (i,j, i+j)

primeSumTo2(10)



def scalarProduct2(xs:Vector[Int], ys:Vector[Int]): Int =
  ( for
    {
     i<- 0 until xs.length
    } yield xs(i)*ys(i)
    ) sum

scalarProduct2(v, w)

def scalarProduct3(xs:Vector[Int], ys:Vector[Int]): Int =
  (for ( (x,y) <- xs zip ys) yield x*y) sum

scalarProduct3(v, w)


def sum( v:Vector[Int] ): Int = v match {
  case h +: t => h + sum(t)
  case _ => 0
}

sum(v)
sum(w)

val a = Array(1,2,3,4)

a map (x=>x*2)

val str = "Hola"

str filter (c => c!='o')
str map (c=>c.toUpper)