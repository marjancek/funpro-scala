val f = "apples" :: "pears" :: "peaches" :: Nil

val n = 3::1::4::1::5::9::2::6::5::Nil


n.tail.tail.tail.tail.head

def sum ( xs: List[Int] ) : Int = xs match {
  case Nil => 0
  case n :: ns => n + sum(ns)
}

sum(n)
sum(n.tail)

def insert( n:Int, xs: List[Int] ) : List[Int]= xs match {
  case Nil => n::Nil
  case y :: ys =>
    if(n <= y) n :: y :: ys
    else y :: insert(n, ys)
}

def isort ( xs: List[Int] ) : List[Int]= xs match {
  case Nil => Nil
  case n :: ns => insert(n, isort(ns))
}

def init[T](list: List[T]): List[T] = list match {
  case List() => throw new Error("init of empty list")
  case List(x) => Nil
  case y :: ys => y :: init(ys)
}

def concat[T](l1: List[T],l2: List[T]): List[T] = l1 match {
  case List() => l2
  case List(x) => x::l2
  case y :: ys => y :: concat(ys, l2)

}

def last[T](list: List[T]): T = list match {
  case List() => throw new Error("init of empty list")
  case List(x) => x
  case y :: ys => last(ys)
}

def reverse[T](list: List[T]): List[T] = list match {
  case List() => list
  case y :: ys => reverse(ys) ++ List(y)
}

def removeAt[T](n: Int, xs: List[T]): List[T] = xs match {
  case List() => xs
  case y :: ys => if( n == 0) ys else y :: removeAt(n-1, ys)
}

def flatten(xs: List[Any]): List[Any] = xs match {
  case List() => Nil
  case (list:List[Any])::rest => flatten(list) ::: flatten(rest)
  case x::xs => x :: flatten(xs)
}

flatten(List(List(1, 1), 2, List(3, List(5, 8))))

val s = isort(n)

init(s)
concat(s,s)
reverse(s)

removeAt(3, s)