
package example.test.intset

abstract class IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
  def union(other: IntSet): IntSet
}

object Empty extends IntSet { // object instead of class; so its singleton
  def contains(x: Int): Boolean = false
  def incl(x: Int) = new NonEmpty(x, Empty, Empty)
  def union(other: IntSet): IntSet = other
  override def toString = "."
}

class NonEmpty(current: Int, left: IntSet, right: IntSet) extends IntSet {

  def contains(x: Int): Boolean =
    if (current == x) true
    else if(current < x) right.contains(x)
    else left.contains(x)

  def incl(x: Int): IntSet =
    if (current < x) new NonEmpty( current, left, right.incl(x) )
    else if (current > x) new NonEmpty( current, left.incl(x), right )
    else this


  def union(other: IntSet): IntSet = {
    if(other == Empty)  this
    else ((left union right) union other ) incl current
  }

  override def toString = "{" + left + "|" + current + "|" + right + "}"
}


object intset {
  def main(args: Array[String]) = {
    val t0 = Empty
    val t1 = new NonEmpty(3, Empty, Empty)
    val t2 = t1 incl 4
    val t3 = t2 incl 1
    val t4 = t3 incl 6
    val t5 = t4 incl 7
    val t6 = t5 incl 7
    val t7 = t6 incl 5
    println(t7)

    val o1 = new NonEmpty(2, Empty, Empty)
    val o2 = o1 incl(8)
    val o3 = o2 incl(9)
    val o4 = o3 incl(10)
    val o5 = o4 incl(0)
    println(o5)

    val u = t7 union o5
    println(u)
  }
}

intset.main( new Array[String](0) )  // run main without parameters