def sum(xs:List[Int]): Int = xs match {
  case Nil => 0
  case h::t => h + sum(t)
}

val xs = List(1,2,3,4,4)
sum(xs)


xs reduceLeft ((x,y)=>x*y)

val ys = List(1.0,2.0,3.0,4.0)

ys reduceLeft ((x,y)=>x/y)

ys reduceRight  ((x,y)=>x/y)

val z = 0.25

(xs foldLeft z) ((x,y)=>x*y)

def concat[T](xs:List[T], ys:List[T]): List[T] =
  (xs foldRight ys) ((x,y)=> x::y)

concat(xs, ys)

def concat2[T](xs:List[T], ys:List[T]): List[T] =
  (xs foldLeft ys) ((x,y)=> y::x)

concat2(xs, ys)

xs.reverse
xs.reverse.reverse