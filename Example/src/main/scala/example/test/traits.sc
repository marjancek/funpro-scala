trait Planar {  // Traits cannot have value parameters
  def hight: Int
  def width: Int
  def surface: Int = hight * width
}

class Shape(lineWidth: Int){
}

class Square(side: Int) extends Shape(0) with Planar{
  def hight: Int = side
  def width: Int = side
}


var s = new Square(3)
s.surface