trait Expr
case class Number(n:Int) extends Expr
case class Sum(a:Expr, b:Expr) extends Expr
case class SumClean(a:Expr, b:Expr) extends Expr
case class Prod(a:Expr, b:Expr) extends Expr
case class Neg(e:Expr) extends Expr

def eval(e:Expr): Int = e match {
  case Number(n) => n
  case Sum(a, b) => eval(a) + eval(b)
  case Prod(a, b) => eval(a) * eval(b)
  case Neg(e) => - eval(e)
}



def show(e:Expr): String = e match{
  case Number(n) => n.toString
  case Sum(a:Number, b:Number) => show(a) + "+" +  show(b)
  case Sum(a:Expr, b:Expr) =>
    (
      if (a.isInstanceOf[Sum] || a.isInstanceOf[Number]) show(a)
      else "(" + show(a) + ")"
    ) + "+" +
    (
      if (b.isInstanceOf[Number] || b.isInstanceOf[Sum]) show(b)
      else  "(" + show(b) + ")"
    )

  case Prod(a:Number, b:Number) => show(a) + "*" +  show(b)
  case Prod(a:Expr, b:Expr) =>
    (
      if (a.isInstanceOf[Prod] || a.isInstanceOf[Number]) show(a)
      else "(" + show(a) + ")"
    ) + "*" +
    (
        if (b.isInstanceOf[Number] || b.isInstanceOf[Prod]) show(b)
        else  "(" + show(b) + ")"
    )

  case Neg(e) => "(-" + show(e) + ")"
}



val s = Sum(Number(2), Number(3) )
val p = Prod(Number(7), s)
val x = Prod(Neg(Number(13)),Prod(Prod(s,p),Neg(Sum(p,s))))

show(s)
eval(s)

show(p)
eval(p)

show(x)
eval(x)

show(Sum(Prod(s,Sum(s,s)), Sum(Number(2) ,p) ) )