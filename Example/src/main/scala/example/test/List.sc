import scala.runtime.Nothing$

trait List[+T] {
  def empty: Boolean
  def head: T
  def tail: List[T]
  def prepend[U >: T](elem:U): List[U] = new Cons[U](elem, this)
  def last : T
  def toString: String
}

object Nil extends List[Nothing]{
  def empty = true
  def head: Nothing = throw new NoSuchElementException("Nil head")
  def tail: Nothing = throw new NoSuchElementException("Nil tail")
  def last: Nothing = throw new NoSuchElementException("Nil tail")
  override def toString = "|"
}

class Cons[T]( val head:T, val tail:List[T]) extends List[T] {
  def empty = false

  override def toString = head + "->" + tail

  override def last = {
    if (tail.isInstanceOf[Nil.type]) head
    else tail.asInstanceOf[Cons].last
  }

}

def singleton[T](elem: T) = new Cons[T](elem, Nil)

val i1 = singleton[Int](1);
val b1 = singleton(true);

val i2 = new Cons(2, i1)
val i3 = new Cons(2, i2)

val i5 = new Cons(1, new Cons(2, new Cons(3, new Cons(4, singleton(5)))))

def showNth[T]( list: List[T], nth: Int) : T=
  if (list.empty) throw new IndexOutOfBoundsException()
  else if (nth==0) list.head
  else showNth(list.tail, nth-1)

showNth(i5, 3)
//showNth(i5, -1)
//showNth(i5, 7)

object List{
  def apply[T](): List[T] = Nil
  def apply[T](a:T): List[T] = new Cons[T](a, apply())
  def apply[T](a:T, b:T): List[T] = new Cons[T](a, apply(b))
  def apply[T](a:T, b:T, c:T): List[T] = new Cons[T](a, apply(b, c))
}

val l = List(1, 2, 3)
l.toString

val k = l.prepend( Nil )
k.toString