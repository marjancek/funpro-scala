var nmenotecnics = Map('2'->"ABC", '3'->"DEF", '4'->"GHI",
  '5'->"JKL", '6'->"MNO", '7'->"PQRS", '8'->"TUV", '9'->"WXYZ")

nmenotecnics('3')

val words = List("JAVA", "KAVA", "MONO", "NONO", "JA", "KA", "NO", "ON", "UB")


val charCode : Map[Char, Char] =
  for ((digit, str)<-nmenotecnics; ltr<-str)
    yield (ltr->digit)

def wordCode(word:String):String =
  word.toUpperCase map charCode
//  (for ( l <- word) yield charCode(l.toUpper) ).toString()


wordCode("Java")


val wordForNum:Map[String, Seq[String]] =
 words groupBy wordCode withDefaultValue(Seq())



def translate(phoneNumber:String) : List[String] =
 wordForNum(phoneNumber).toList

translate("6666")

def encode(number: String) : Set[List[String]] =
  if(number.isEmpty) Set(List())
  else
    (for
      {
        split <- 1 to number.length
        word <- wordForNum(number take split)
        rest <- encode(number drop split)
      } yield word::rest).toSet


encode("66665282") map (_ mkString "-")

encode("5282") map (_ mkString "-")
