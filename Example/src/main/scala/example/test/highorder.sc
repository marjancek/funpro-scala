def scaleList(xs:List[Double], factor: Double): List[Double] = xs match{
  case Nil => xs
  case y::ys => y*factor :: scaleList(ys, factor)
}

scaleList(List(2.3, 3.4, 4.5), 2.0)


def mapList[T,U](xs:List[T], fun: (T => U)): List[U] = xs match{
  case Nil => Nil
  case h::t => fun(h) :: mapList(t, fun)
}

mapList[Double, Double](List(2.3, 3.4, 4.5), (x =>2.0*x) )

mapList[Int, Double](List(2, 3, 4), (x =>math.sqrt(x)) )

def filter[T](xs:List[T], f:(T=>Boolean)): List[T] = xs match{
  case Nil => Nil
  case h::t => if(f(h)) h :: filter(t, f) else filter(t, f)
}

filter[Int](List(-1, 1, -2, 2, -3, 3, -4, 4), (x=>x>0))

filter[Int](List(-1, 1, -2, 2, -3, 3, -4, 4), (x=>math.abs(x-x/2-x/2)>0))

filter[String](List("Pear", "Peach", "Cherry"), (x=>x.compareTo("Orange")>0))


def pack[T](xs:List[T]): List[List[T]] = xs match {
  case Nil => Nil
  case y :: ys => {
    val (first, second) = xs span (a=> a==y)
    first::pack(second)
  }
}

pack(List(1, 1, 1, 2, 3,3, 3, 3, 4, 4, 5, 6, 2))

def encode[T](xs:List[T]): List[(T, Int)] = {
  pack(xs) map (x => (x.head, x.length))
}


encode(List(1, 1, 1, 2, 3,3, 3, 3, 4, 4, 5, 6, 2))

encode(List('a','a','b','c','c','c','e','e'))

val list = List(1, 1, 1, 2, 3,3, 3, 3, 4, 4, 5, 6, 2)

list filter (x=>x<=2)
list filterNot (x=>x<=2)
list partition (x=>x<=2)
list takeWhile (x=>x<=2)
list dropWhile (x=>x>2)
list span (x=>x<=2)

