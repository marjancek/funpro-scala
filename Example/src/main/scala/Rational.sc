package week3

class Rational( x: Int, y:Int) {
  require(y!=0, "Denominator cannot be zero.")

  def this(x:Int) = this(x,1)  // convenient constructor for integers

  private val g = gcd(x,y)
  def numer = x / g
  def denom = y / g

  assert(gcd(numer,denom)==1, "Improper fraction")

  def + (r:Rational) = new Rational(numer*r.denom+r.numer*denom,denom*r.denom)

  override def toString = numer + "/" + denom

  def unary_- = new Rational(-numer,denom)

  def - (r:Rational) = this + -r

  private def gcd(a:Int, b:Int): Int = if(b==0) a else gcd(b,a%b)

  def < (r: Rational): Boolean = numer*r.denom < r.numer*denom

  def max(r:Rational) = if (this < r) r else this

  def * (r:Rational) = new Rational(numer*r.numer,denom*r.denom)

  def / (r:Rational) = new Rational(numer*r.denom,denom*r.numer)

  def toDouble = 1.0 * numer / denom

}

val r1 = new Rational(1,2)
val r2 = new Rational(1,3)
println(r1.denom)
val r3 = r1 + r2

-r3

val x = new Rational(1,3)
val y = new Rational(5,7)
val z = new Rational(3,2)

x + y + z

y + y


x < y
x.max(y)

val p = new Rational(13)

r2 / r1 * p

r2.toDouble