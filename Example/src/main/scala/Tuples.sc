val x = List(1,8,6,7,9)
val y = List(2,3,4,5,8,10)


object mergesort {

  def msort[T](xs:List[T])(implicit ord: Ordering[T]) : List[T] = {
    val n = xs.length / 2
    if(n==0)  xs
    else {
      def merge[T](xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
          case (List(), ys) => ys
          case (xs, List()) => xs
          case (a :: as, b :: bs) =>
            if (ord.lt(b, a)) b :: merge(xs, bs)
            else a :: merge(as, ys)
        }
      val (first, second) = xs splitAt n
      merge(msort(first)(ord), msort(second)(ord))
    }
  }
}

mergesort.msort(x)

mergesort.msort(List("Pear", "Peach", "Cherry"))