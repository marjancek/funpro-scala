type Word = String

val w1 = "yes"
val w2 = "man"
val s = List(w1, w2)


val s1= List(1, 3, 4)

def remove(w:Word, l:List[Word]):List[Word] = {for (v<-l if v!=w) yield v}
s
remove("yes", s)
w1 :: s


val m = List(List("1", "2", "3"), List("a", "b"))

def merge(w:Word, l:List[List[Word]]):List[List[Word]] = l match{
  case Nil => List(List(w))
  case x::xs => (w::x) :: (if(xs==List()) Nil else merge(w, xs))
}


def permut(l:List[Word]):List[List[Word]] = l match {
  case Nil => Nil
  case _ => {
    (for (i <- l) yield merge(i, permut(remove(i, l))) ) flatten
  }
}

def explode(l:List[List[Word]]):List[List[Word]] = l match{
  case Nil=> List(List())
  case x::xs => permut(x) ++ explode(xs)
}

merge("1", List(List()))

permut(List("1"))
permut(List("1", "2"))

permut(List("1", "2", "3"))

explode(m)
