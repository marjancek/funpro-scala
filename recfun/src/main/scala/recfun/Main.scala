package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if(c==0 || c==r) 1
      else pascal(c-1, r-1 ) + pascal(c, r-1 )
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def loop(level: Int, chars: List[Char]): Boolean = {
        if ( level < 0 ) false
        else if ( chars.isEmpty ) level==0
        else {
          if ( chars.head == '(' )  loop(level+1, chars.tail)
          else if ( chars.head == ')' )  loop(level-1, chars.tail)
          else loop(level, chars.tail)
        }
      }
      loop(0, chars)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {

      def innerCount(money: Int, coins: List[Int]): Int = {

        if (coins.isEmpty || money < 0) 0
        else if (money == 0) 1
        else if (money - coins.head == 0) 1
        else innerCount(money - coins.head, coins) +
          //countChange( money-coins.head, coins.tail) +
          innerCount(money, coins.tail)
      }

      innerCount(money, coins.sortWith(_<_))
    }
  }
