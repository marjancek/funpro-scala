object sqrt2 {
  def abs(x: Double) = if(x<0) -x else x

  def sqrt(x: Double) = {
    def improve(guess: Double) = (guess + x / guess) / 2.0

    def goodEnough(guess: Double) = abs(x - guess * guess) / x < 0.001

    def sqrtIter(guess: Double): Double =
      if (goodEnough(guess)) guess
      else sqrtIter(improve(guess))

    sqrtIter(1.0)
  }

  abs(-1)

  sqrt(4)
  sqrt(2)
  sqrt(1)
  sqrt(0.5)
  sqrt(0.001)
  sqrt(0.1e-20)
  sqrt(1.0e20)
  sqrt(1.0e50)

}
