object factorial {

  def factorial(n: Int): Int = {

    def loop(accum: Int, n: Int): Int = {
      if (n == 0) accum
      else loop(accum * n, n - 1)
    }

    loop(1, n)

  }

  factorial(8)
}