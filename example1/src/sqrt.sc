object sqrt {
  def abs(x: Double) = if(x<0) -x else x

  def improve(guess: Double, x: Double) = (guess + x/guess) / 2.0

  def goodEnough(guess: Double, x: Double) = abs(x - guess*guess)/x < 0.001

  def sqrtIter(guess: Double, x: Double) : Double =
    if(goodEnough(guess, x)) guess
    else sqrtIter(improve(guess, x), x)

  def sqrt(x: Double) : Double = sqrtIter(1.0, x)

  abs(-1)
  improve(1.0, 2)
  goodEnough(2, 4)
  sqrt(4)
  sqrt(2)
  sqrt(1)
  sqrt(0.5)
  sqrt(0.001)
  sqrt(0.1e-20)
  sqrt(1.0e20)
  sqrt(1.0e50)

}
